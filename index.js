let posts = []
let count = 0

const showPosts = (posts) => 
{
	let posts_entries = ''

	posts.forEach((post) => {
		posts_entries += `
		<div id="post-${post.id}">
		    <h3 id="post-title-${post.id}">${post.title}</h3>
		    <p id="post-body-${post.id}">${post.body}</p>
		    <button onclick="editPost('${post.id}')">Edit</button>
		    <button onclick="deletePost('${post.id}')">Delete</button>
		</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = posts_entries
}

// Add new post
document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	event.preventDefault()

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})

	// Increment the ID for each new post
	count++

	// show the updated posts after adding a new post
	showPosts(posts)

	alert('Successfuly added a new post!')
})

// Responsible for editing post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
}

document.querySelector('#form-edit-post').addEventListener('submit', (event) => {
    event.preventDefault();

    // Loop through array to find the match
    for (let i = 0; i < posts.length; i++) {
        // The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
        // Therefore, it is necesary to convert the Number to a String first.

        // If match has been found, then proceed with assigning the new values to the existing post
        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;
            
            // show the updated list of posts once this specific post has been updated
            showPosts(posts);
            alert('Successfully updated.');
            
            break;
        }
    }
})

// Responsible for deleting a post
const deletePost = (id) => {
    posts.splice(id, 1)

    showPosts(posts)

	alert('Successfuly deleted a post!')
}